import { PureComponent } from "react";
import { StyledModal,ModalBckg,ModalContent,StyledParagraph,StyledHeader,StyledH,StyledButton } from "./styled";

class Modal extends PureComponent {
  render () {
    const { title, text, hideFn, id, hasCloseButton, actions } = this.props;
    const optionalCeneterd = {
      'justifyContent': hasCloseButton ? 'space-between' : 'center',
    }
    return (
      <ModalBckg onClick={() => hideFn(id)}>
        <StyledModal onClick={e => e.stopPropagation()}>
          <StyledHeader style={optionalCeneterd}>
            <StyledH>{title}</StyledH>
            {hasCloseButton && <StyledButton onClick={() => hideFn(id)}>&#10761;</StyledButton>}
          </StyledHeader>
          <ModalContent>
           <StyledParagraph>{text}</StyledParagraph>
            <div>
              {actions?.length > 0 && actions[0]}
              {actions?.length > 1 && actions[1]}
            </div>
          </ModalContent>
        </StyledModal>
      </ModalBckg>
    )
  }
}

export default Modal;