import styled from "styled-components"

export const StyledButton = styled.button `
background-color: ${props => props.$bgColor || "#BF4F74"};
padding:10px 15px;
margin-right:10px;

`