import { PureComponent } from "react";
import { StyledButton } from "./styled";

class Button extends PureComponent {
  render() {
    const { onClick, text, backgroundColor} = this.props
    return <StyledButton onClick={onClick} $bgColor={backgroundColor} >{text}</StyledButton>
  } 
}

export default Button;